import { useDispatch } from 'react-redux';

const Header = ({lastname: string}: any) => {
  const dispatch = useDispatch();

  const action = {
    type: 'UPDATE_NAME',
    name: 'Ashi'
  };

  return (
    <div>
      <h1>Header</h1>

      <button onClick={() => dispatch(action)}>Update</button>
    </div>
  );
}

export default Header;
