import { useDispatch } from 'react-redux';

function OneComponent() {
  const dispatch = useDispatch();

  const action = {
    type: 'UPDATE_NAME',
    name: 'New name'
  };
  
  return (
    <div>
      <h2>Component One</h2>

      <button onClick={() => dispatch(action)}>Update again</button>
    </div>
  );
}

export default OneComponent;
