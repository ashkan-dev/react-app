import { useSelector } from "react-redux";

function FourComponent() {
  const data = useSelector((state: any) => state);

  return (
    <div>
      <h3>Component Four</h3>
      {!!data ? <h4>{data.name}</h4> : null}
    </div>
  );
}

export default FourComponent;
