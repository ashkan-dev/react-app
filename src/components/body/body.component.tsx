import FourComponent from '../four.component';
import OneComponent from '../one.component';
import ThreeComponent from '../three.component';
import TwoComponent from '../two.component';

const Body = ({lastname: string}: any) => {
  return (
    <div>
      <h1>Body</h1>

      <OneComponent />

      <TwoComponent />

      <ThreeComponent />

      <FourComponent />
    </div>
  );
}

export default Body;