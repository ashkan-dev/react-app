import { createStore } from "redux";
import { userReducer } from "./reducers/user.reducer";

export const store = createStore(userReducer);