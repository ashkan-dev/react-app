export function userReducer(state = {}, action: any): { name?: string } {
  switch (action.type) {
    case "UPDATE_NAME":
      return {
        name: action.name,
      };
    default:
      return {};
  }
}
