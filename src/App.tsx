import logo from "./logo.svg";
import "./App.css";
import Body from "./components/body/body.component";
import Footer from "./components/footer/footer.component";
import Header from "./components/header/header.component";
import { Provider } from "react-redux";
import { store } from "./state-manager/store";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

          <Header lastname="Coder" />

          <Body lastname="Coder" />

          <Footer lastname="Coder" />
        </header>
      </div>
    </Provider>
  );
}

export default App;
